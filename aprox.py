#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys


def compute_trees(trees, base_trees, fruit_per_tree, reduction):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(min_trees, max_trees):
    productions = []

    for trees in range(min_trees, max_trees + 1):
        production = compute_trees(trees)
        productions.append((trees, production))
    return productions


def read_arguments():
    if len(sys.argv) != 6:
        print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
        sys.exit()

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:
        print("All arguments must be integers")
        sys.exit()
    return base_trees, fruit_per_tree, reduction, min, max



def main():
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    best_production = max(p for _, p in productions)
    best_trees = [t for t, p in productions if p == best_production][0]

    print(f"Best production: {best_production}, for {best_trees} trees")


if __name__ == '__main__':
    main()
